import {Container , Grid , TextField , Button , List , ListItem} from "@mui/material"
import { useSelector , useDispatch } from "react-redux"

import { inputChangeHandler } from "../actions/task.actions"
import { taskAddClicked } from "../actions/task.actions"
import { taskToggleClicked } from "../actions/task.actions"
const Task = () =>{

    const dispath = useDispatch();
    const {inputString,taskList} = useSelector((reduxData) =>{
        return reduxData.taskReducer
    })

    
    const inputTaskChangeHandler = (event) =>{
        dispath(inputChangeHandler(event.target.value));
    }

    const addButtonClickHandler = () =>{
        dispath(taskAddClicked());
    }

    const taskClickToggleHandler = (index) =>{
        dispath(taskToggleClicked(index));
    }
    return (
        <Container>
            {/* Grid container là dòng , item là cột  */}
            <Grid container mt ={5} alignItems="center" textAlign="center">
                <Grid item xs={12} md={6} lg={8} sm={12}>
                    <TextField label = "Input task here" variant="outlined" fullWidth value={inputString} onChange={inputTaskChangeHandler}/>
                </Grid>
                <Grid item xs={12} md={6} lg={4} sm={12}>
                    <Button onClick={addButtonClickHandler} variant="contained">Add Task</Button>
                </Grid>
            </Grid>
            <Grid>
                <List>
                    {taskList.map((element , index) =>{
                        return <ListItem onClick={() => taskClickToggleHandler(index)} key={index} style={{color : element.status ? "green" : "red"}}> {index + 1}. {element.name}</ListItem>
                    })}
                </List>
            </Grid>
        </Container>
    )
}

export default Task

